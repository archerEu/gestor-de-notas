# proyecto
-  Proyecto de gestor de notas con ajax

```sh
├───application
│   ├───cache
│   ├───config
│   ├───controllers
│   ├───core
│   ├───errors
│   ├───helpers
│   ├───hooks
│   ├───language
│   │   ├───english
│   │   └───spanish
│   ├───libraries
│   ├───logs
│   ├───models
│   ├───third_party
│   └───views
├───css
│   └───plugins
├───font-awesome
│   ├───css
│   ├───fonts
│   ├───less
│   └───scss
├───fonts
├───img
├───js
├───proyecto
│   ├───.git
│   │   ├───hooks
│   │   ├───info
│   │   ├───lfs
│   │   │   ├───objects
│   │   │   │   └───logs
│   │   │   └───tmp
│   │   │       └───objects
│   │   ├───logs
│   │   │   └───refs
│   │   │       ├───heads
│   │   │       └───remotes
│   │   │           └───origin
│   │   ├───objects
│   │   │   ├───02
│   │   │   ├───04
│   │   │   ├───05
│   │   │   ├───06
│   │   │   ├───07
│   │   │   ├───08
│   │   │   ├───0a
│   │   │   ├───0c
│   │   │   ├───0d
│   │   │   ├───10
│   │   │   ├───11
│   │   │   ├───14
│   │   │   ├───15
│   │   │   ├───19
│   │   │   ├───1a
│   │   │   ├───1b
│   │   │   ├───1c
│   │   │   ├───1d
│   │   │   ├───1e
│   │   │   ├───1f
│   │   │   ├───22
│   │   │   ├───23
│   │   │   ├───24
│   │   │   ├───25
│   │   │   ├───26
│   │   │   ├───27
│   │   │   ├───28
│   │   │   ├───29
│   │   │   ├───2a
│   │   │   ├───2b
│   │   │   ├───2d
│   │   │   ├───2e
│   │   │   ├───2f
│   │   │   ├───30
│   │   │   ├───32
│   │   │   ├───33
│   │   │   ├───34
│   │   │   ├───35
│   │   │   ├───38
│   │   │   ├───39
│   │   │   ├───3c
│   │   │   ├───3d
│   │   │   ├───3e
│   │   │   ├───3f
│   │   │   ├───40
│   │   │   ├───43
│   │   │   ├───44
│   │   │   ├───45
│   │   │   ├───46
│   │   │   ├───47
│   │   │   ├───4a
│   │   │   ├───4b
│   │   │   ├───4c
│   │   │   ├───4d
│   │   │   ├───4e
│   │   │   ├───50
│   │   │   ├───53
│   │   │   ├───57
│   │   │   ├───58
│   │   │   ├───5a
│   │   │   ├───5b
│   │   │   ├───5c
│   │   │   ├───5e
│   │   │   ├───5f
│   │   │   ├───60
│   │   │   ├───61
│   │   │   ├───64
│   │   │   ├───66
│   │   │   ├───67
│   │   │   ├───69
│   │   │   ├───6a
│   │   │   ├───6c
│   │   │   ├───6d
│   │   │   ├───6e
│   │   │   ├───6f
│   │   │   ├───70
│   │   │   ├───72
│   │   │   ├───73
│   │   │   ├───79
│   │   │   ├───7a
│   │   │   ├───7b
│   │   │   ├───7c
│   │   │   ├───82
│   │   │   ├───83
│   │   │   ├───85
│   │   │   ├───86
│   │   │   ├───87
│   │   │   ├───89
│   │   │   ├───8a
│   │   │   ├───8e
│   │   │   ├───90
│   │   │   ├───92
│   │   │   ├───93
│   │   │   ├───96
│   │   │   ├───98
│   │   │   ├───9a
│   │   │   ├───9b
│   │   │   ├───9c
│   │   │   ├───9e
│   │   │   ├───a0
│   │   │   ├───a1
│   │   │   ├───a3
│   │   │   ├───a4
│   │   │   ├───a5
│   │   │   ├───a6
│   │   │   ├───a8
│   │   │   ├───a9
│   │   │   ├───ae
│   │   │   ├───af
│   │   │   ├───b0
│   │   │   ├───b1
│   │   │   ├───b3
│   │   │   ├───b4
│   │   │   ├───b6
│   │   │   ├───b7
│   │   │   ├───b8
│   │   │   ├───b9
│   │   │   ├───bb
│   │   │   ├───bc
│   │   │   ├───bd
│   │   │   ├───c1
│   │   │   ├───c3
│   │   │   ├───c5
│   │   │   ├───c7
│   │   │   ├───c8
│   │   │   ├───c9
│   │   │   ├───ca
│   │   │   ├───cc
│   │   │   ├───cd
│   │   │   ├───ce
│   │   │   ├───cf
│   │   │   ├───d1
│   │   │   ├───d3
│   │   │   ├───d8
│   │   │   ├───da
│   │   │   ├───dd
│   │   │   ├───de
│   │   │   ├───df
│   │   │   ├───e0
│   │   │   ├───e2
│   │   │   ├───e3
│   │   │   ├───e5
│   │   │   ├───e6
│   │   │   ├───e7
│   │   │   ├───e8
│   │   │   ├───e9
│   │   │   ├───ed
│   │   │   ├───ef
│   │   │   ├───f0
│   │   │   ├───f1
│   │   │   ├───f2
│   │   │   ├───f3
│   │   │   ├───f5
│   │   │   ├───f6
│   │   │   ├───f7
│   │   │   ├───f8
│   │   │   ├───fa
│   │   │   ├───fb
│   │   │   ├───fc
│   │   │   ├───fd
│   │   │   ├───info
│   │   │   └───pack
│   │   └───refs
│   │       ├───heads
│   │       ├───remotes
│   │       │   └───origin
│   │       └───tags
│   ├───application
│   │   ├───cache
│   │   ├───config
│   │   ├───controllers
│   │   ├───core
│   │   ├───errors
│   │   ├───helpers
│   │   ├───hooks
│   │   ├───language
│   │   │   ├───english
│   │   │   └───spanish
│   │   ├───libraries
│   │   ├───logs
│   │   ├───models
│   │   ├───third_party
│   │   └───views
│   ├───css
│   ├───img
│   ├───js
│   │   └───gritter
│   │       ├───css
│   │       ├───images
│   │       └───js
│   └───system
│       ├───core
│       ├───database
│       │   └───drivers
│       │       ├───cubrid
│       │       ├───mssql
│       │       ├───mysql
│       │       ├───mysqli
│       │       ├───oci8
│       │       ├───odbc
│       │       ├───pdo
│       │       ├───postgre
│       │       ├───sqlite
│       │       └───sqlsrv
│       ├───fonts
│       ├───helpers
│       ├───language
│       │   └───english
│       └───libraries
│           ├───Cache
│           │   └───drivers
│           └───javascript
└───system
    ├───core
    ├───database
    │   └───drivers
    │       ├───cubrid
    │       ├───mssql
    │       ├───mysql
    │       ├───mysqli
    │       ├───oci8
    │       ├───odbc
    │       ├───pdo
    │       ├───postgre
    │       ├───sqlite
    │       └───sqlsrv
    ├───fonts
    ├───helpers
    ├───language
    │   └───english
    └───libraries
        ├───Cache
        │   └───drivers
        └───javascript
```

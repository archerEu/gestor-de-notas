

    $(function() {
        $( "#datePickerDesde" ).datepicker();
        $( "#datePickerDesde" ).datepicker( "option", "dateFormat", 'dd-mm-yy' );
    });

    $(function() {
        $( "#datePickerHasta" ).datepicker();
        $( "#datePickerHasta" ).datepicker( "option", "dateFormat", 'dd-mm-yy' );
    });

	$(function() {
		$( "#dataPicker" ).datepicker();
		$( "#dataPicker" ).datepicker( "option", "dateFormat", 'dd-mm-yy' );
	});

	$(document).ready(function(){
        var fechaActual=cualEsLaFechaActual();
		$('#dataPicker').val(cualEsLaFechaActual);
        $('#calendar-s').val(cualEsLaFechaActual);
        $('#datePickerDesde').val(fechaActual);
        $('#datePickerHasta').val(fechaActual);
	});



  $(function($){
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
});

    function cualEsLaFechaActual(){
        var hoy = new Date();
        dia = hoy.getDate();
        mes = hoy.getMonth();
        mes++;
        if(mes<10){
            mes='0'+mes;
        }
        anio= hoy.getFullYear();
        return String(dia+"-"+mes+"-"+anio);
    
    }

    function fechaReciente(){
        var hoy = new Date();
        dia = hoy.getDate();
        mes = hoy.getMonth();
        mes++;
        if(mes<10){
            mes='0'+mes;
        }
        anio= hoy.getFullYear();
        return String(dia+"-"+mes+"-"+anio);
    }



  $("#btnAceptar").click(
    function(){
      guardarIncidentes();
    }
  );
  
  function guardarIncidentes(){
    $.post(
      base_url + "Controlador/guardarIncidentes",
      {
        idNIncidente:$("#idNIncidente").val(),
        idClasificacion:$("#idClasificacion").val(),
        idCalendar:$("#calendar-s").val(),
        idEstado:$("input:radio[name ='radioIdEstado']:checked").val(),
        idMotivo:$("#idMotivo").val()
      },
      function(datos){
        $("#divNotificacion").html(datos.mensaje);
        $("#idNIncidente").val(datos.numero_doc);
        $("#idMotivo").val("");
      },'json'
    );
  }

  $("#btnModalBuscarIncidente").click(
    function() {
     buscarIncidente();
    }
  );

  $("#btnCerrar1").click(
    function() {
     $("#textBuscarIncidente").val("");
     $("#idDivMensajeTabla").empty();

    }
  );

  $("#btnCerrar2").click(
    function() {
     $("#textBuscarIncidente").val("");
     $("#idDivMensajeTabla").empty();

    }
  );

  function buscarIncidente () {
    $("#idDivTabla").empty();
    $.post(
      base_url + "Controlador/buscarIncidente",
      {
        idIncidente:$("#textBuscarIncidente").val()
      },
      function(datos) {
      var t="";
      var mensaje="";
      t="<table id='tablaIncidente' class='table table-bordered table-hover table-striped'  data-toggle='table' data-pagination='true' data-search=true'>";
            t=t+"<caption>Listado de Incidente</caption>";
            t=t+"<thead>";
              t=t+"<tr>";
                        t=t+"<th>Nº Incidente</th>";
                        t=t+"<th>Motivo incidente</th>";
                        t=t+"<th align='rigth'>fecha_incidente</th>";
                        t=t+"<th width='80px'>Seleccionar</th>";
              t=t+"</tr>";
            t=t+"</thead>";
      t=t+"<tbody>";
      var objJson = jQuery.parseJSON(datos);
        $.each(objJson,function(index,valor) {
            t=t+"<tr id='tr_"+valor.id_incidente+"'>";
                t=t+"<td>"+valor.id_incidente+"</td>";
                t=t+"<td>"+valor.motivo_incidente+"</td>";
                t=t+"<td>"+valor.fecha_incidente+"</td>";
                t=t+"<td  align='center'>";
                t=t+"<input id='ck_"+valor.id_incidente+"' type='checkbox'  name='option3' value='Cheese' onclick='obtenerIncidente("+valor.id_incidente+")' >";
                t=t+"</td>";
            t=t+"</tr>";
        });
      t=t+"</tbody>";
      t=t+"</table>";
      $.each(objJson,function(index,valor) {
        mensaje = valor.mensaje;
      });
      $("#idDivTabla").html(t);
      $("#idDivMensajeTabla").html(mensaje);
      });
  }

  function obtenerIncidente (incidente) {
    alert("A seleccionado el incidente"+incidente);
    $("#idBuscarIncidente").val(incidente);
  }

  $("#btnCambiarIncidente").click(
    function() {
     actualizarIncidente();
    }
  );

  function actualizarIncidente () {
    $.post(
      base_url + "Controlador/actualizarIncidente",
      {
        idBuscarIncidente:$("#idBuscarIncidente").val(),
        idClasificacion:$("#idClasificacion").val(),
        dataPicker:$("#dataPicker").val(),
        idEstado:$("input:radio[name ='radioIdEstado']:checked").val(),
        idTextMotivo:$("#idTextMotivo").val()
      },
      function(datos) {
        $("#idDivMensajeCambios").html(datos.mensaje);
        $("#idTextMotivo").val("");
        $("#idBuscarIncidente").val("");
      },'json'
      );
  }


  function buscarIncidenteClasificacion () {
    $.post(
      base_url + "Controlador/buscarIncidenteClasificacion",
      {
        idMotivoBuscar:$("#idMotivoClasificacion").val(),
        idClasificacionBuscar:$("#idClasificacionBuscar").val()
      },
      function(datos) {
        var t="";
        var mensaje="";
        t="<table id='idTablaPrioridad' class='table table-bordered table-hover table-striped'  data-toggle='table' data-pagination='true' data-search=true'>";
              t=t+"<caption>Listado de Incidente</caption>";
              t=t+"<thead>";
                t=t+"<tr>";
                          t=t+"<th>Nº Incidente</th>";
                          t=t+"<th>Motivo</th>";
                          t=t+"<th align='rigth'>fecha_incidente</th>";
                t=t+"</tr>";
              t=t+"</thead>";
        t=t+"<tbody>";
        var objJson = jQuery.parseJSON(datos);
          $.each(objJson,function(index,valor) {
              t=t+"<tr id='tr_"+valor.id_incidente+"'>";
                  t=t+"<td>"+valor.id_incidente+"</td>";
                  t=t+"<td>"+valor.motivo_incidente+"</td>";
                  t=t+"<td>"+valor.fecha_incidente+"</td>";
              t=t+"</tr>";
          });
        t=t+"</tbody>";
        t=t+"</table>";
        $.each(objJson,function(index,valor) {
        mensaje = valor.mensaje;
        });
        $("#idDivTablaPrioridad").html(t);
        $("#idMensajeBuscarMensaje").html(mensaje);
      });
  }
  
  $("#btnBuscarIncidenteClasificacion").click(
    function() {
     buscarIncidenteClasificacion();
    }
  );

  function buscarTodosDatos(){
    $("#idDivTablaDatos").empty();
    $.post(
      base_url + "Controlador/buscarTodosDatos",
      {
        idIncidente:$("#idBuscarIncidenteDatos").val(),
        idEstado:$("input:radio[name ='radioIdEstado']:checked").val(),
        datePickerDesde:$("#datePickerDesde").val(),
        datePickerHasta:$("#datePickerHasta").val()
      },
      function(datos){
        var t="";
        var mensaje="";
        t="<table id='idTablaDatos' class='table table-bordered table-hover table-striped'  data-toggle='table' data-pagination='true' data-search=true'>";
              t=t+"<caption>Listado de Incidente</caption>";
              t=t+"<thead>";
                t=t+"<tr>";
                          t=t+"<th>Nº Incidente</th>";
                          t=t+"<th>Usuario</th>";
                          t=t+"<th>Motivo</th>";
                          t=t+"<th align='rigth'>fecha_incidente</th>";
                          t=t+"<th>Estado</th>";
                          t=t+"<th>Clasificación</th>";
                t=t+"</tr>";
              t=t+"</thead>";
        t=t+"<tbody>";
        var objJson = jQuery.parseJSON(datos);
          $.each(objJson,function(index,valor) {
              t=t+"<tr id='tr_"+valor.id_incidente+"'>";
                  t=t+"<td>"+valor.id_incidente+"</td>";
                  t=t+"<td>"+valor.id_usuario+"</td>";
                  t=t+"<td>"+valor.motivo_incidente+"</td>";
                  t=t+"<td>"+valor.fecha_incidente+"</td>";
                  t=t+"<td>"+valor.nombre_estado+"</td>";
                  t=t+"<td>"+valor.nombre_clasificacion+"</td>";
              t=t+"</tr>";
          });
        t=t+"</tbody>";
        t=t+"</table>";
        $.each(objJson,function(index,valor) {
        mensaje = valor.mensaje;
        });
        $("#idDivTablaDatos").html(t);
        $("#idDivMensajeDatos").html(mensaje);
      }
    );
  }

  $("#btnBuscarTodoDatos").click(
    function() {
     buscarTodosDatos();
    }
  );





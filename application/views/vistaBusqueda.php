
        <div id="page-wrapper">

            <div class="container-fluid">
                
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Búsqueda<small> Información general</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Búsqueda
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- End  Heading-->
                

                <!-- Panel -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Panel de Búsqueda</h3>
                    </div>
                    <div class="panel-body">
                        <!-- Row -->
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <!-- Gruop input -->
                            <div class="form-group">
                                <label>Nº Incidente</label>
                                <input id="idBuscarIncidenteDatos" type="text" class="form-control" >  
                            </div>
                            
                            <label>Estado</label>
                            <div class="form-group">
                                
                                <div class="col-lg-4">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="radioIdEstado" value="1" checked>Abierto
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="radioIdEstado" value="2">Resuelto
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="radioIdEstado" value="3">Cerrado
                                        </label>
                                    </div> 
                                </div>
                                <div class="col-lg-4">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="radioIdEstado" value="4">En Proceso
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="radioIdEstado" value="5">Programado
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="radioIdEstado" value="6">Sin Solución
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button id="btnBuscarTodoDatos" style="width: 100%" type="button" class="btn btn-info">Buscar</button>
                            </div>

                        <!-- End Group -->
                    </div>

                    <div class="col-lg-6 col-md-6">
                        <!-- Start Accordin -->
                          <div class="col-lg-4">
                            <div class="input-group">
                                <label for="desdePedido">Desde</label>

                                <div class="controls">
                                    <div class="input-group">
                                        <input id="datePickerDesde" type="text" class="date-picker form-control" placeholder="dd/mm/aaaa"  readonly="" />
                                        <label for="datePickerDesde" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                          </div>
                          <div class="col-lg-4">
                            <div class="input-group">
                              <label for="hastaPedido">Hasta</label>
                                <div class="controls">
                                    <div class="input-group">
                                        <input id="datePickerHasta" type="text" class="date-picker form-control"  placeholder="dd/mm/aaaa" readonly="" />
                                        <label for="datePickerHasta" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                          </div>
                        <!-- End Accordion -->
                    </div>
                </div>
                <!-- End Panel -->

                <br>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Datos de los incidente</h3>
                    </div>
                    <div class="panel-body"> 
                        <!-- Input Group -->
                        <div id="idDivTablaDatos" class="form-group">
                            <table id="" class="table table-bordered table-hover table-striped" data-toggle="table" data-pagination="true">
                            <caption>Datos</caption>
                            <thead>
                              <tr>
                                <th>Nº Incidente</th>
                                <th>Usuario</th>
                                <th>Motivo</th>
                                <th>Fecha Incidente</th>
                                <th>Estado</th>
                                <th>Clasificación</th>
                              </tr>
                            </thead>
                            <tbody>
                                <tr>
                            <?php
                            foreach ($data_tabla as $row) {
                            ?>
                                <tr>
                                  <td><?php echo $row->id_incidente ?></td>
                                  <td><?php echo $row->id_usuario; ?></td>
                                  <td><?php echo $row->motivo_incidente; ?></td>
                                  <td><?php echo $row->fecha_incidente; ?></td>
                                  <td><?php echo $row->nombre_estado; ?></td>
                                  <td><?php echo $row->nombre_clasificacion; ?></td>
                                <?php
                            }
                                ?>
                                </tr>
                            </tbody>
                            </table>
                        </div>
                        <div id="idDivMensajeDatos" class="form-group">
                            
                        </div>
                        <!-- End Group -->
                    </div>
                </div>
                
                <!-- Modal -->
                
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Busqueda</h4>
                      </div>
                      <div class="modal-body">
                        <div class="form-group">
                            <div class="col-lg-3">
                                <label>Motivo</label>
                                <input id="idMotivoClasificacion" type="text" class="form-control" >  
                            </div>
                            <div class="col-lg-3">
                                <label>Prioridad</label>
                                <select id="idClasificacionBuscar" class="form-control">
                                    <option value="1">Alta</option>
                                    <option value="2">Media</option>
                                    <option value="3">Baja</option>
                                </select>
                            </div>
                            <div class="col-lg-3">
                                <button id="btnBuscarIncidenteClasificacion" style="margin-top: 21px;" class="btn btn-info">Buscar</button>
                            </div>
                        </div>
                        <div class="form-group" id="idDivTablaPrioridad">
                            <table id="idTablaPrioridad" class="table table-bordered table-hover table-striped" data-toggle="table" data-pagination="true">
                            <caption>Incidente Conocidos</caption>
                            <thead>
                              <tr>
                                <th>Nº Incidente</th>
                                <th>Motivo</th>
                                <th>Fecha Incidente</th>
                              </tr>
                            </thead>
                            <tbody>
                                <tr>
                            <?php
                            foreach ($data_get as $row) {
                            ?>
                                <tr>
                                  <td><?php echo $row->id_incidente ?></td>
                                  <td><?php echo $row->motivo_incidente; ?></td>
                                  <td><?php echo $row->fecha_incidente; ?></td>
                                <?php
                            }
                                ?>
                                </tr>
                            </tbody>
                            </table>
                        </div>
                        <div class="form-group" id="idMensajeBuscarMensaje">
                               
                        </div>     
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default"  data-dismiss="modal">Cerrar</button>
                      </div>
                    </div>
                  </div>
                </div>

                <!-- End mobal-->
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->
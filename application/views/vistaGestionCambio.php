
        <div id="page-wrapper">

            <div id="container-page" class="container-fluid">
                
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Gestión de Cambios<small> Información general</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Gestión de Cambios
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- End  Heading-->
                

                <!-- Panel -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Panel de Gestión de Cambios</h3>
                    </div>
                    <div class="panel-body">
                        
                <!-- Row -->
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <!-- Gruop input -->
                            <div class="form-group">
                                <label>Nº Incidente</label>
                                <input id="idBuscarIncidente" type="text" class="form-control" readonly>  
                            </div>
                            <div class="form-group">
                                <button id="btnBuscarIncidente" style="width: 100%" class="btn btn-info" data-toggle="modal" data-target="#idMobalBuscarIncidente">Buscar Incidente</button>
                            </div>
                            <div class="form-group">
                                <label>Prioridad</label>
                                <select id="idClasificacion" class="form-control">
                                    <option value="1">Alta</option>
                                    <option value="2">Media</option>
                                    <option value="3">Baja</option>
                                </select>
                            </div>
                            <div class="form-group input-group">
                              <label for="hastaPedido">Fecha</label>
                                <div class="controls">
                                    <div class="input-group">
                                        <input id="dataPicker" type="text" class="date-picker form-control"  placeholder="dd/mm/aaaa" readonly />
                                        <label for="dataPicker" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <label>Estado</label>
                            <div class="form-group">
                                
                                <div class="col-lg-4">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="radioIdEstado" value="1" checked>Abierto
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="radioIdEstado" value="2">Resuelto
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="radioIdEstado" value="3">Cerrado
                                        </label>
                                    </div> 
                                </div>
                                <div class="col-lg-4">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="radioIdEstado" value="4">En Proceso
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="radioIdEstado" value="5">Programado
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="radioIdEstado" value="6">Sin Solución
                                        </label>
                                    </div>
                                </div>
                            </div>
                        <!-- End Group -->
                    </div>

                    <div class="col-lg-6 col-md-6">
                        <!-- Start Accordin -->
                        <p class="help-block">Elemento de Ayuda</p>
                        <div id="accordion" class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">1. Alta</a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse">
                                    <div class="panel-body">

                                        <p> su solucion debe ser generada rapidamente porque su error o su problema genera mas errores en el sistema y evita el funcionamiento optimo del servicio. <a href="http://www.tutorialrepublic.com/html-tutorial/" target="_blank">Learn more.</a></p>

                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">2. Media</a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse in">
                                    <div class="panel-body">

                                        <p>media: su solucion necesita verificacion y revision... es un problema o error que pude pasar a prioridad alta si no es tratado y solucionado. <a href="http://www.tutorialrepublic.com/twitter-bootstrap-tutorial/" target="_blank">Learn more.</a></p>

                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">3. Baja</a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse">
                                    <div class="panel-body">

                                        <p>baja: aunque es un problema su pronta solucion no es de prioridad para atencion al cliente, este problema seguira el curso regular y los dias previamente estipulados para su solucion o su revision. <a href="http://www.tutorialrepublic.com/css-tutorial/" target="_blank">Learn more.</a></p>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Accordion -->
                    </div>
                </div>
                <!-- Row -->

                <!-- Panel -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Panel del Motivo</h3>
                    </div>
                    <div class="panel-body"> 
                        <!-- Input Group -->    
                                          
                        <div class="form-group">
                                <textarea id="idTextMotivo" class="form-control" rows="10"></textarea>
                        </div>
                        <div class="form-group">
                            <button id="btnCambiarIncidente" style="width:100%" type="button" class="btn btn-lg btn-primary">Cambiar</button>
                        </div>
                        <div id="idDivMensajeCambios" class="form-group">
                            
                        </div>
                        <!-- End Group -->
                    </div>
                </div>
                <!-- End Panel -->


                <!-- Modal -->
                
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Busqueda</h4>
                      </div>
                      <div class="modal-body">
                        <div class="form-group">
                            <div class="col-lg-3">
                                <label>Motivo</label>
                                <input id="idMotivoClasificacion" type="text" class="form-control" >  
                            </div>
                            <div class="col-lg-3">
                                <label>Prioridad</label>
                                <select id="idClasificacionBuscar" class="form-control">
                                    <option value="1">Alta</option>
                                    <option value="2">Media</option>
                                    <option value="3">Baja</option>
                                </select>
                            </div>
                            <div class="col-lg-3">
                                <button id="btnBuscarIncidenteClasificacion" style="margin-top: 21px;" class="btn btn-info">Buscar</button>
                            </div>
                        </div>
                        <div class="form-group" id="idDivTablaPrioridad">
                            <table id="idTablaPrioridad" class="table table-bordered table-hover table-striped" data-toggle="table" data-pagination="true">
                            <caption>Incidente Conocidos</caption>
                            <thead>
                              <tr>
                                <th>Nº Incidente</th>
                                <th>Motivo</th>
                                <th>Fecha Incidente</th>
                              </tr>
                            </thead>
                            <tbody>
                                <tr>
                            <?php
                            foreach ($data_get as $row) {
                            ?>
                                <tr>
                                  <td><?php echo $row->id_incidente ?></td>
                                  <td><?php echo $row->motivo_incidente; ?></td>
                                  <td><?php echo $row->fecha_incidente; ?></td>
                                <?php
                            }
                                ?>
                                </tr>
                            </tbody>
                            </table>
                        </div>
                        <div class="form-group" id="idMensajeBuscarMensaje">
                               
                        </div>     
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default"  data-dismiss="modal">Cerrar</button>
                      </div>
                    </div>
                  </div>
                </div>

                <!-- End mobal-->

                <!-- inicio modal -->
                <div id="idMobalBuscarIncidente" class="modal fade" role="dialog">
                    <div class="modal-dialog" id="idMobalC">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button id="btnCerrar1" type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Buscar Incidente</h4>
                            </div>
                            <div class="modal-body">
                                <!--FORM -->
                                
                                <div class="form-group">
                                    <input type="text" class="form-control" id="textBuscarIncidente">
                                </div>
                                <div class="form-group">
                                    <button id="btnModalBuscarIncidente" type="button" class="btn btn-info" style="width:100%">Buscar</button>
                                </div>
                                <div class="form-group" id="idDivTabla">
                                    <table id="tablaPedido" class="table table-bordered table-hover table-striped" data-toggle="table" data-pagination="true">
                                        <caption>Seleccione Incidente</caption>
                                        <thead>
                                          <tr>
                                            <th>Nº Incidente</th>
                                            <th>Motivo</th>
                                            <th>Fecha Incidente</th>
                                            <th width="80px">Seleccionar</th>
                                          </tr>

                                        </thead>
                                        </table>
                                </div>
                                <div class="form-group" id="idDivMensajeTabla">
                                    
                                </div>
                                <div class="form-group">
                                    <button id="btnIncidenAceptar" style="width: 50%" data-dismiss="modal" class="btn btn-default">Aceptar</button>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" id="btnCerrar2" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End modal -->
                
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->
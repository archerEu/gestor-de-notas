
        <div id="page-wrapper">

            <div class="container-fluid">
                
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Gestión de Incidente<small> Información general</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Gestión de Incidente
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- End  Heading-->

                <!-- Panel -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Panel Listado</h3>
                    </div>
                    <div class="panel-body"> 
                        <div class="form-group" id="idDivTablaPrioridad">
                            <table id="idTablaPrioridad" class="table table-bordered table-hover table-striped" data-toggle="table" data-pagination="true">
                            <caption>Incidente Conocidos</caption>
                            <thead>
                              <tr>
                                <th>Nº Incidente</th>
                                <th>Motivo</th>
                                <th>Fecha Incidente</th>
                              </tr>
                            </thead>
                            <tbody>
                                <tr>
                            <?php
                            foreach ($data_get as $row) {
                            ?>
                                <tr>
                                  <td><?php echo $row->id_incidente ?></td>
                                  <td><?php echo $row->motivo_incidente; ?></td>
                                  <td><?php echo $row->fecha_incidente; ?></td>
                                <?php
                            }
                                ?>
                                </tr>
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- End Panel -->
                
            </div>
            <!-- /.container-fluid -->
                    <!-- inicio modal -->
                
                <!-- Modal -->
                
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Busqueda</h4>
                      </div>
                      <div class="modal-body">
                        <div class="form-group">
                            <div class="col-lg-3">
                                <label>Motivo</label>
                                <input id="idMotivoClasificacion" type="text" class="form-control" >  
                            </div>
                            <div class="col-lg-3">
                                <label>Prioridad</label>
                                <select id="idClasificacionBuscar" class="form-control">
                                    <option value="1">Alta</option>
                                    <option value="2">Media</option>
                                    <option value="3">Baja</option>
                                </select>
                            </div>
                            <div class="col-lg-3">
                                <button id="btnBuscarIncidenteClasificacion" style="margin-top: 21px;" class="btn btn-info">Buscar</button>
                            </div>
                        </div>
                        <div class="form-group" id="idDivTablaPrioridad">
                            <table id="idTablaPrioridad" class="table table-bordered table-hover table-striped" data-toggle="table" data-pagination="true">
                            <caption>Incidente Conocidos</caption>
                            <thead>
                              <tr>
                                <th>Nº Incidente</th>
                                <th>Motivo</th>
                                <th>Fecha Incidente</th>
                              </tr>
                            </thead>
                            <tbody>
                                <tr>
                            <?php
                            foreach ($data_get as $row) {
                            ?>
                                <tr>
                                  <td><?php echo $row->id_incidente ?></td>
                                  <td><?php echo $row->motivo_incidente; ?></td>
                                  <td><?php echo $row->fecha_incidente; ?></td>
                                <?php
                            }
                                ?>
                                </tr>
                            </tbody>
                            </table>
                        </div>
                        <div class="form-group" id="idMensajeBuscarMensaje">
                               
                        </div>     
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default"  data-dismiss="modal">Cerrar</button>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- End modal -->
        </div>
        <!-- /#page-wrapper -->
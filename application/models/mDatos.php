<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mdatos extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }


    public function obtenerNumeroDoc() {
        $this->db->select_max('id_incidente','ultimo_id');
        $res1 = $this->db->get('incidente');
        $num = 0;
        $id_n = 0;
        if ($res1->num_rows() == 0) {
            $num = 1;
        }else{
            foreach ($res1->result() as $record) {
                $num = (int)$record->ultimo_id;
            }
        }
        $id_n = (int)$num + 1;
        return $id_n;
    }

    function guardarIncidente($motivo_incidente, $fecha_incidente) {
        $datos = array(
            "motivo_incidente"       => $motivo_incidente,
            "fecha_incidente" => $fecha_incidente
        );
        $this->db->insert('incidente', $datos);
        $numero_doc = $this->db->insert_id();
        return $numero_doc;
    }

    function guardarHistorial($id_usuario,$incidente,$id_clasificacion,$id_estado)
    {
        $datos = array(
            "id_usuario"       => $id_usuario,
            "id_codigo" => $incidente,
            "id_clasificacion" => $id_clasificacion,
            "id_estado" => $id_estado,
        );
        $this->db->insert('historial', $datos);   
    }

    function buscarIncidente($id_incidente)
    {
        $this->db->select('*');
        $this->db->where('id_incidente',$id_incidente);
        $query = $this->db->get('incidente');
        return $query;
    }

    function actualizarIncidente($idIncidente,$idTextMotivo,$fecha)
    {
        $data = array(
            'motivo_incidente' => $idTextMotivo,
            'fecha_incidente' => $fecha
        );
        $this->db->where('id_incidente',$idIncidente);
        $this->db->update('incidente',$data);
        return true;
    }

    function actualizarHistorial($idIncidente,$idClasificacion,$idEstado)
    {
        $data = array(
            'id_clasificacion' => $idClasificacion,
            'id_estado' => $idEstado
        );
        $this->db->where('id_historial',$idIncidente);
        $this->db->update('historial',$data);
        return true;
    }

    function buscarIncidenteMotivoClasificacion($idMotivo,$idClasificacion)
    {
       $sql = "SELECT i.id_incidente,i.motivo_incidente, i.fecha_incidente from incidente i 
       JOIN historial h ON i.id_incidente = h.id_historial 
       JOIN clasificacion c ON h.id_clasificacion = c.id_clasificacion 
       WHERE c.id_clasificacion = ".$idClasificacion." 
       AND i.motivo_incidente LIKE '%".$idMotivo."%' 
       OR i.motivo_incidente LIKE '% ".$idMotivo." %'
       OR i.motivo_incidente LIKE '% ".$idMotivo."'";
       return $this->db->query($sql);
    } 

    function buscarIncidentenPorClasificacion($idClasificacion)
    {
       $this->db->select('id_incidente,motivo_incidente,fecha_incidente');
       $this->db->from('incidente');
       $this->db->join('historial','incidente.id_incidente = historial.id_historial');
       $this->db->join('clasificacion','historial.id_clasificacion = clasificacion.id_clasificacion');
       $this->db->where('clasificacion.id_clasificacion',$idClasificacion);
       $query = $this->db->get();
       return $query;
    }

    function obtenerIncidendes()
    {
        $query = $this->db->get('incidente');
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    function obtenerTodoIncidentesEstadoClasificacion()
    {
        $this->db->select('incidente.id_incidente,usuario.id_usuario,incidente.motivo_incidente,incidente.fecha_incidente,estado.nombre_estado,clasificacion.nombre_clasificacion');
        $this->db->from('incidente');
        $this->db->join('historial','incidente.id_incidente = historial.id_historial');
        $this->db->join('clasificacion','historial.id_clasificacion = clasificacion.id_clasificacion');
        $this->db->join('estado','historial.id_estado = estado.id_estado');
        $this->db->join('usuario','historial.id_usuario = usuario.id_usuario');
        $query = $this->db->get();
        return $query->result();
    }

    function buscarIncidentePorDatos($idIncidente,$idEstado,$desde,$hasta)
    {
        $sql = "SELECT i.id_incidente,u.id_usuario, i.motivo_incidente, i.fecha_incidente, e.nombre_estado, c.nombre_clasificacion FROM incidente i 
        JOIN historial h ON i.id_incidente = h.id_historial 
        JOIN clasificacion c ON h.id_clasificacion = c.id_clasificacion 
        JOIN estado e ON e.id_estado = ".$idEstado." 
        JOIN usuario u ON h.id_usuario = u.id_usuario 
        WHERE i.id_incidente = ".$idIncidente." AND i.fecha_incidente >= '".$desde."' AND i.fecha_incidente <= '".$hasta."'";
        return $this->db->query($sql);
    }

    function buscarPorDatos($idEstado,$desde,$hasta)
    { 
        $sql = "SELECT i.id_incidente,u.id_usuario, i.motivo_incidente, i.fecha_incidente, e.nombre_estado, c.nombre_clasificacion FROM incidente i 
        JOIN historial h ON i.id_incidente = h.id_historial 
        JOIN clasificacion c ON h.id_clasificacion = c.id_clasificacion 
        JOIN estado e ON e.id_estado = ".$idEstado." 
        JOIN usuario u ON h.id_usuario = u.id_usuario 
        WHERE i.fecha_incidente >= '".$desde."' AND i.fecha_incidente <= '".$hasta."'";
        return $this->db->query($sql);
    }

       /*$sql = "SELECT i.id_incidente,i.motivo_incidente, i.fecha_incidente from incidente i
        JOIN historial h ON i.id_incidente = h.id_historial
        JOIN clasificacion c ON h.id_clasificacion = c.id_clasificacion
        WHERE c.id_clasificacion =".$idClasificacion;
        return $this->db->query($sql);
        */
       
              /*$sql = "SELECT i.id_incidente,i.motivo_incidente, i.fecha_incidente from incidente i 
       JOIN historial h ON i.id_incidente = h.id_historial 
       JOIN clasificacion c ON h.id_clasificacion = c.id_clasificacion 
       WHERE c.id_clasificacion = ".$idClasificacion." and i.motivo_incidente LIKE '".$idMotivo."'";
       return $this->db->query($sql);
        */
       
       /**
        *
        *SELECT u.id_usuario, i.motivo_incidente, i.fecha_incidente, e.nombre_estado, c.nombre_clasificacion FROM
        *incidente i 
        *JOIN historial h ON i.id_incidente = h.id_historial 
        *JOIN clasificacion c ON h.id_clasificacion = c.id_clasificacion 
        *JOIN estado e ON h.id_codigo = e.id_estado 
        *JOIN usuario u ON h.id_usuario = u.id_usuario 
        *WHERE i.id_incidente = 1 
        * 
        */


        /**
         *
         * SELECT u.id_usuario, i.motivo_incidente, i.fecha_incidente, e.nombre_estado, c.nombre_clasificacion FROM incidente i 
         * JOIN historial h ON i.id_incidente = h.id_historial 
         * JOIN clasificacion c ON h.id_clasificacion = c.id_clasificacion 
         * JOIN estado e ON h.id_estado = e.id_estado 
         * JOIN usuario u ON h.id_usuario = u.id_usuario 
         * 
        *SELECT u.id_usuario, i.motivo_incidente, i.fecha_incidente, e.nombre_estado, c.nombre_clasificacion FROM incidente i 
        *JOIN historial h ON i.id_incidente = h.id_historial 
        *JOIN clasificacion c ON h.id_clasificacion = c.id_clasificacion 
        *JOIN estado e ON e.id_estado = 3 
        *JOIN usuario u ON h.id_usuario = u.id_usuario 
        *WHERE i.id_incidente = 1 AND i.fecha_incidente >= '2015-11-20' AND i.fecha_incidente <= '2015-11-25' 
         */

/*
SELECT u.id_usuario, i.motivo_incidente, i.fecha_incidente, e.nombre_estado, c.nombre_clasificacion FROM incidente i 
JOIN historial h ON i.id_incidente = h.id_historial 
JOIN clasificacion c ON h.id_clasificacion = c.id_clasificacion 
JOIN estado e ON h.id_codigo = e.id_estado 
JOIN usuario u ON h.id_usuario = u.id_usuario 
WHERE i.id_incidente = 1 AND i.fecha_incidente >= '2015-11-21' AND i.id_incidente <= '2015-11-21' AND e.id_estado = 3


SELECT u.id_usuario, i.motivo_incidente, i.fecha_incidente, e.nombre_estado, c.nombre_clasificacion FROM incidente i 
JOIN historial h ON i.id_incidente = h.id_historial 
JOIN clasificacion c ON h.id_clasificacion = c.id_clasificacion 
JOIN estado e ON e.id_estado = 3 
JOIN usuario u ON h.id_usuario = u.id_usuario 
WHERE i.id_incidente = 1 AND i.fecha_incidente >= '2015-11-20' AND i.fecha_incidente <= '2015-11-25' 

SELECT u.id_usuario, i.motivo_incidente, i.fecha_incidente, e.nombre_estado, c.nombre_clasificacion FROM incidente i JOIN historial h ON i.id_incidente = h.id_historial JOIN clasificacion c ON h.id_clasificacion = c.id_clasificacion JOIN estado e ON e.id_estado = 3 JOIN usuario u ON h.id_usuario = u.id_usuario WHERE i.id_incidente = 1 
*/
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Controlador extends CI_Controller {
	function __construct() {
        parent::__construct();
        $this->load->model("mDatos");
        
    }

	public function index()
	{
		if($this->session->userdata('logged_in'))
        {
          $session_data = $this->session->userdata('logged_in');
           $data['username'] = $session_data['usuario'];
           $rol = $session_data['rol'];
			     $respuesta         = $this->mDatos->obtenerNumeroDoc();
		       $data['cantidad'] = $respuesta;
           $data['data_get'] = $this->mDatos->obtenerIncidendes();
            if ($rol==1) {
	            $this->load->view('header',$data);
	            $this->load->view('contenidoAdministrador');
				      $this->load->view('footer');	
            }else{
	            $this->load->view('headerOperador',$data);
	            $this->load->view('contenidoAdministrador');
				      $this->load->view('footer');	
            }
			
        } else {
            redirect('controladorLogin', 'refresh');
        }
	}

     public function Gestion()
     {
		  if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['usuario'];
            $data['data_get'] = $this->mDatos->obtenerIncidendes();
            $rol = $session_data['rol'];
            if ($rol==1) {
	            $this->load->view('header',$data);
	            $this->load->view('vistaGestionCambio');
				      $this->load->view('footer');	
            }else{
	            $this->load->view('headerOperador',$data);
	            $this->load->view('vistaGestionCambio');
				      $this->load->view('footer');	
            }
			
        } else {
            redirect('controladorLogin', 'refresh');
        }
     }

  	public function Busqueda()
     {
		if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['usuario'];
            $rol = $session_data['rol'];
            $data['data_get'] = $this->mDatos->obtenerIncidendes();
            $data['data_tabla'] = $this->mDatos->obtenerTodoIncidentesEstadoClasificacion();
            if ($rol==1) {
	            $this->load->view('header',$data);
	            $this->load->view('vistaBusqueda');
				      $this->load->view('footer');	
            }else{
	            $this->load->view('headerOperador',$data);
	            $this->load->view('vistaBusqueda');
				      $this->load->view('footer');	
            }

        } else {
            redirect('controladorLogin', 'refresh');
        }
     }

     public function Listados1()
     {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['usuario'];
            $data['data_get'] = $this->mDatos->obtenerIncidendes();
            $data['data_tabla'] = $this->mDatos->obtenerTodoIncidentesEstadoClasificacion();
            $rol = $session_data['rol'];
            if ($rol==1) {
              $this->load->view('header',$data);
              $this->load->view('vistaListado1');
              $this->load->view('footer');  
            }else{
              $this->load->view('headerOperador',$data);
              $this->load->view('vistaListado1');
              $this->load->view('footer');  
            }
      
        } else {
            redirect('controladorLogin', 'refresh');
        }
     }

     public function Listados2()
     {
       if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['usuario'];
            $data['data_get'] = $this->mDatos->obtenerIncidendes();
            $rol = $session_data['rol'];
            if ($rol==1) {
              $this->load->view('header',$data);
              $this->load->view('vistaListado2');
              $this->load->view('footer');  
            }else{
              $this->load->view('headerOperador',$data);
              $this->load->view('vistaListado2');
              $this->load->view('footer');  
            }
      
        } else {
            redirect('controladorLogin', 'refresh');
        }
     }

     public function guardarIncidentes()
     {
     	$incidente = $this->input->post('idNIncidente');
     	$clasificacion = $this->input->post('idClasificacion');
     	$calendar = $this->input->post('idCalendar');
     	$estado = $this->input->post('idEstado');
     	$motivo = $this->input->post('idMotivo');
      $session_data = $this->session->userdata('logged_in');
      $data = $session_data['usuario'];

     	$fecha = $this->convertirFecha($calendar);
     	$mensaje  = '';
        $num = 0;
     	if ($motivo == "") {
     		$mensaje = '<div class="alert alert-warning"> Se ha producido un problema, no ha agregado el motivo . </div>';
     	}else{
     		$respuesta = (int)$this->mDatos->guardarIncidente($motivo,$fecha);
            if (empty($respuesta) == null) {
                $this->mDatos->guardarHistorial($data,$respuesta,$clasificacion,$estado);
                $mensaje = '<div class="alert alert-success"> El incidente se ha agregado correctamente. </div>';
            }
            $num = (int)$respuesta + 1; 
     	}
        
        if ($num == 0) {
            $num = (int)$this->mDatos->obtenerNumeroDoc()+1;
        }

     	echo json_encode(array("mensaje" => $mensaje,"numero_doc" => $num));
     }

  	public function convertirFecha($dato) {
  		$y = substr($dato, 6, 4);
  		$m = substr($dato, 3, 2);
  		$d = substr($dato, 0, 2);
  		return $y."-".$m."-".$d;
  	}
    
    public function buscarIncidente()
    {
        $id_incidente = $this->input->post('idIncidente');
        $respuesta = $this->mDatos->buscarIncidente($id_incidente);
        if ($respuesta->num_rows() == 0) {
            $arreglo[] = array(
                'id_incidente'      => '',
                'motivo_incidente'  => '',
                'fecha_incidente'   => '',
                'mensaje' => '<div class="alert alert-warning"> Se ha producido un problema, incidente no existe. </div>'
            );
        }else{

            foreach ($respuesta->result() as $record ) {
                $arreglo[] = array(
                    'id_incidente'      => $record->id_incidente,
                    'motivo_incidente'  => $record->motivo_incidente,
                    'fecha_incidente'   => $record->fecha_incidente,
                    'mensaje' => '<div class="alert alert-success"> El incidente se ha encontrado. </div>'
                );
            }

        }
        echo json_encode($arreglo);
    }

    public function actualizarIncidente()
    {
        $idIncidente     = $this->input->post('idBuscarIncidente');
        $idClasificacion = $this->input->post('idClasificacion');
        $dataPicker      = $this->input->post('dataPicker');
        $idEstado        = $this->input->post('idEstado');
        $idTextMotivo    = $this->input->post('idTextMotivo');

        $fecha = $this->convertirFecha($dataPicker);

        $mensaje = '';
        if ($idIncidente == "" || $idTextMotivo == "") {
            $mensaje = '<div class="alert alert-warning"> Se ha producido un problema, no ha agregado motivo o incidente . </div>' ; 
        }else{
            $respuesta1 = $this->mDatos->actualizarIncidente($idIncidente,$idTextMotivo,$fecha);
            $respuesta2 = $this->mDatos->actualizarHistorial($idIncidente,$idClasificacion,$idEstado);
            if ($respuesta1 == true && $respuesta2 == true) {
                $mensaje = '<div class="alert alert-success"> El incidente se ha modificado correctamente. </div>';
            } 
        }
        echo json_encode(array("mensaje" => $mensaje));
    }

    public function buscarIncidenteClasificacion()
    {
      $idMotivo = $this->input->post('idMotivoBuscar');
      $idClasificacion = $this->input->post('idClasificacionBuscar');

      if ($idMotivo == "") {
        $respuesta = $this->mDatos->buscarIncidentenPorClasificacion($idClasificacion);
        if ($respuesta->num_rows() == 0) {
            $arreglo[] = array(
              'id_incidente'      => '',
              'motivo_incidente'  => '',
              'fecha_incidente'   => '',
              'mensaje' => '<div class="alert alert-warning"> Se ha producido un problema, incidente no existe. </div>'
            );
        }else{
          foreach ($respuesta->result() as $record ) {
            $arreglo[] = array(
                'id_incidente'      => $record->id_incidente,
                'motivo_incidente'  => $record->motivo_incidente,
                'fecha_incidente'   => $record->fecha_incidente,
                'mensaje' => '<div class="alert alert-success"> El incidente se ha encontrado. </div>'
            );
          } 
        }
      }else{
        $respuesta = $this->mDatos->buscarIncidenteMotivoClasificacion($idMotivo,$idClasificacion);
        if ($respuesta->num_rows() == 0) {
            $arreglo[] = array(
                'id_incidente'      => '',
                'motivo_incidente'  => '',
                'fecha_incidente'   => '',
                'mensaje' => '<div class="alert alert-warning"> Se ha producido un problema, incidente no existe. </div>'
            );
        }else{
          foreach ($respuesta->result() as $record ) {
            $arreglo[] = array(
                'id_incidente'      => $record->id_incidente,
                'motivo_incidente'  => $record->motivo_incidente,
                'fecha_incidente'   => $record->fecha_incidente,
                'mensaje' => '<div class="alert alert-success"> El incidente se ha encontrado. </div>'
          );
          }
        }
      }
      echo json_encode($arreglo);
    }

    public function buscarTodosDatos()
    {
        $idIncidente      = $this->input->post('idIncidente');
        $idEstado         = $this->input->post('idEstado');
        $datePickerDesde  = $this->input->post('datePickerDesde');
        $datePickerHasta  = $this->input->post('datePickerHasta');

        $desde = $this->convertirFecha($datePickerDesde);
        $hasta = $this->convertirFecha($datePickerHasta);

        if ($idIncidente == "") {
          $respuesta = $this->mDatos->buscarPorDatos($idEstado,$desde,$hasta);
          if ($respuesta->num_rows() == 0) {
          $arreglo[] = array(
            'id_incidente'      => '', 
            'id_usuario'        => '',
            'motivo_incidente'  => '',
            'fecha_incidente'   => '',
            'nombre_estado'     => '',
            'nombre_clasificacion' => '',
            'mensaje' => '<div class="alert alert-warning"> Se ha producido un problema, incidente no existe. </div>'
          );
          }else{
          foreach ($respuesta->result() as $record ) {
            $arreglo[] = array(
                'id_incidente'      => $record->id_incidente, 
                'id_usuario'        => $record->id_usuario,
                'motivo_incidente'  => $record->motivo_incidente,
                'fecha_incidente'   => $record->fecha_incidente,
                'nombre_estado'     => $record->nombre_estado,
                'nombre_clasificacion' => $record->nombre_clasificacion
            );
          }
         }
        }else{
          $respuesta = $this->mDatos->buscarIncidentePorDatos($idIncidente,$idEstado,$desde,$hasta);

          if ($respuesta->num_rows() == 0) {
            $arreglo[] = array(
              'id_incidente'      => '', 
              'id_usuario'        => '',
              'motivo_incidente'  => '',
              'fecha_incidente'   => '',
              'nombre_estado'     => '',
              'nombre_clasificacion' => '',
              'mensaje' => '<div class="alert alert-warning"> Se ha producido un problema, incidente no existe. </div>'
            );
          }else{
          foreach ($respuesta->result() as $record ) {
            $arreglo[] = array(
                'id_incidente'      => $record->id_incidente, 
                'id_usuario'        => $record->id_usuario,
                'motivo_incidente'  => $record->motivo_incidente,
                'fecha_incidente'   => $record->fecha_incidente,
                'nombre_estado'     => $record->nombre_estado,
                'nombre_clasificacion' => $record->nombre_clasificacion
            );
          }
         }

        }
        echo json_encode($arreglo);
    }

  }


/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */